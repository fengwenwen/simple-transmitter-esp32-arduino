
#include <U8g2lib.h>

// Define stepper motor connections and steps per revolution:
#define dirPin 14 //
#define stepPin 27 //
#define clockwiseDir LOW  //TODO
#define anticlockwiseDir HIGH  //TODO
#define stepsPerRevolution 1000
#define stepsSize 1000
#define targetOffsetStepMax 40000
#define targetOffsetStepMin 1000
#define stepPeriod 200
#define resetStepPeriod 200

#define powerPin 23

#define springPositionSteps 40000 //252000
#define initialTargetOffsetSteps 20000 //157500

#define buttonPinA 15//进行
#define buttonPinB 2
#define buttonPinC 4
#define buttonPinD 5

#define clipPinA 12
#define clipPinB 13

int prevButtonA = 1;
int prevButtonB = 1;
int prevButtonC = 1;

int targetOffsetSteps = initialTargetOffsetSteps;//157500

char strLEDCharBuffer[32];

void reset_stepper();
void moveTo(int steps);
void openClip();

U8G2_SH1106_128X64_NONAME_F_HW_I2C u8g2(U8G2_R2, /* reset=*/ U8X8_PIN_NONE);

void setup() {

  u8g2.begin();
  u8g2.setContrast(255);
  u8g2.clearBuffer();          // clear the internal memory
  u8g2.setFont(u8g2_font_helvR08_te);  // choose a suitable font
  delay(1000);

  // Declare pins as output:
  pinMode(stepPin, OUTPUT);
  pinMode(dirPin, OUTPUT);
  pinMode(clipPinA, OUTPUT);
  pinMode(clipPinB, OUTPUT);

  digitalWrite(clipPinA, LOW);
  digitalWrite(clipPinB, LOW);

  pinMode(buttonPinA, INPUT);
  pinMode(buttonPinB, INPUT);
  pinMode(buttonPinC, INPUT);
  pinMode(buttonPinD, INPUT);

  pinMode(powerPin, OUTPUT);
  digitalWrite(powerPin, 1);


  Serial.begin(115200);
  Serial.println("Ready!");

  delay(10);

  closeClip();
  //openClip();//测试//放开夹子
  delay(500);
  reset_stepper();////初始化下拉复位，以及获得发射箭
  //动作结束，默认为上拉方向


  u8g2.clearBuffer();          // clear the internal memory
  u8g2.setCursor(20,20);
  u8g2.print("IVE TY");  // write something to the internal memory

  u8g2.sendBuffer();
  delay(100); //delay reserved
}



void loop() {
 delay(10);
  int buttonStateA = digitalRead(buttonPinA);
  int buttonStateB = digitalRead(buttonPinB);
  int buttonStateC = digitalRead(buttonPinC);

  if (buttonStateA == 0 && buttonStateA != prevButtonA) {//按下a按钮，进入发射流程
    //
    Serial.println("load arrow....");

    u8g2.clearBuffer();
    u8g2.setFont(u8g2_font_ncenB14_tr);
    u8g2.drawStr(0, 20, "load arrow....");
    u8g2.sendBuffer();
    delay(1000);

    // close clip
    Serial.println("Close clip");
    u8g2.clearBuffer();
    u8g2.drawStr(0, 20, "Close clip");
    u8g2.sendBuffer();

    closeClip();//关紧夹子
    //    delay(100);

    digitalWrite(dirPin, clockwiseDir); //clockwise://设置为下拉反向
    moveTo(targetOffsetSteps);//到达预设发射位置
    delay(500);

    // open clip
    Serial.println("Open clip");
    u8g2.clearBuffer();
    u8g2.drawStr(0, 20, "Open clip");
    u8g2.sendBuffer();

    openClip();//放开夹子，发射
    //    delay(100);

    //return to spring
    digitalWrite(dirPin, anticlockwiseDir); //clockwise://设置为上拉方向
    moveTo(targetOffsetSteps);//返回发射位置
    digitalWrite(dirPin, clockwiseDir); //clockwise://设置为下拉方向

  }
  else if (buttonStateB == 0 && buttonStateB != prevButtonB) {//设置发射位置，上拉,移动
    if ((targetOffsetSteps + stepsSize) < targetOffsetStepMax)
    {
      targetOffsetSteps += stepsSize;
      digitalWrite(dirPin, anticlockwiseDir); //clockwise://设置为上拉方向
      moveTo(stepsSize);
      digitalWrite(dirPin, clockwiseDir); //clockwise://设置为下拉方向
      
      sprintf(strLEDCharBuffer, "Offset: %d", targetOffsetSteps);

      Serial.println(targetOffsetSteps);
      u8g2.clearBuffer();
      u8g2.drawStr(0, 20, strLEDCharBuffer);
      u8g2.sendBuffer();

    }
  }
  else if (buttonStateC == 0 && buttonStateC != prevButtonC) {//设置发射位置,下拉,移动
    if ((targetOffsetSteps - stepsSize) > targetOffsetStepMin)
    {
      targetOffsetSteps -= stepsSize;
      digitalWrite(dirPin, clockwiseDir); //clockwise://设置为下拉方向
      moveTo(stepsSize);
      
      sprintf(strLEDCharBuffer, "Offset: %d", targetOffsetSteps);

      Serial.println(targetOffsetSteps);
      u8g2.clearBuffer();
      u8g2.drawStr(0, 20, strLEDCharBuffer); // ("this is %d", x)
      u8g2.sendBuffer();
      digitalWrite(dirPin, clockwiseDir); //clockwise://设置为下拉方向
    }
  }
  Serial.println(targetOffsetSteps);
  //预防重复操作
  prevButtonA = buttonStateA;
  prevButtonB = buttonStateB;
  prevButtonC = buttonStateC;

  return;

}

void reset_stepper() {//初始化下拉复位，以及获得发射箭
  Serial.println("Reset....");

  u8g2.clearBuffer();
  u8g2.setFont(u8g2_font_ncenB14_tr);
  u8g2.drawStr(0, 20, "Reset....");
  u8g2.sendBuffer();


  digitalWrite(dirPin, clockwiseDir); //clockwise://设置为下拉方向//电机的顺时针
  //进行复位程序
  while (digitalRead(buttonPinD) != 0) {//当没有碰到底座按钮
    digitalWrite(stepPin, HIGH);
    delayMicroseconds(resetStepPeriod);
    digitalWrite(stepPin, LOW);
    delayMicroseconds(resetStepPeriod);
    //交流电
  }
  delay(500);
  //进行上拉，获得箭
  digitalWrite(dirPin, anticlockwiseDir); //anticlockwise//设置为上拉方向
  moveTo(springPositionSteps);//到达发射位置

  Serial.println("Ready.");

  u8g2.clearBuffer();
  u8g2.setFont(u8g2_font_ncenB14_tr);
  u8g2.drawStr(0, 20, "Ready.");
  u8g2.sendBuffer();

 // openClip();//放开夹子//虽然之前已经是放开状态
  delay(100);
}

void moveTo(int steps) {
  Serial.print("Moving to ");

  u8g2.clearBuffer();
  u8g2.setFont(u8g2_font_ncenB14_tr);
  u8g2.drawStr(0, 20, "Moving to ");
  u8g2.sendBuffer();

  Serial.print(steps);
  Serial.println("...");
  for (int i = 0; i < steps; i++) {//当没有到达步骤时间时
    digitalWrite(stepPin, HIGH);
    delayMicroseconds(stepPeriod);
    digitalWrite(stepPin, LOW);
    delayMicroseconds(stepPeriod);
    //交流电
    /*
    u8g2.clearBuffer();
    u8g2.drawStr(0, 20, "step ");
    u8g2.sendBuffer();
  */
  }
  Serial.println("Movement completed.");

  u8g2.clearBuffer();
  u8g2.drawStr(0, 20, "Movement");
  u8g2.drawStr(60, 20, "completed.");
  u8g2.sendBuffer();
}

void openClip() {//放开夹子
  Serial.println("openClip");
  digitalWrite(clipPinA, HIGH); //clockwise:
  delay(100);
  digitalWrite(clipPinA, LOW); //clockwise:
  delay(100);
  
  //交流电
}

void closeClip() {//关紧夹子
  Serial.println("closeClip");
  digitalWrite(clipPinB, HIGH); //clockwise:
  delay(100);
  digitalWrite(clipPinB, LOW); //clockwise:
  delay(100);
  //交流电
}
